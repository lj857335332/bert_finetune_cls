import argparse

import sys
sys.path.append("./")

from bert_finetune_cls.trainer import Trainer
from bert_finetune_cls.utils import init_logger, load_tokenizer, read_prediction_text, set_seed, MODEL_CLASSES, MODEL_PATH_MAP
from bert_finetune_cls.data_loader import load_and_cache_examples


def main(args):
    # 初始化日志
    init_logger()
    # 设置随机数种子
    set_seed(args)
    # 加载分词模型
    tokenizer = load_tokenizer(args)
    # 加载训练、验证、测试dataset
    train_dataset = load_and_cache_examples(args, tokenizer, mode="train")
    dev_dataset = load_and_cache_examples(args, tokenizer, mode="dev")
    test_dataset = load_and_cache_examples(args, tokenizer, mode="test")

    trainer = Trainer(args, train_dataset, dev_dataset, test_dataset)
    # 训练
    if args.do_train:
        trainer.train()
    # 评估
    if args.do_eval:
        # 加载模型
        trainer.load_model()
        # 在验证集与测试集上进行评估
        trainer.evaluate("dev")
        trainer.evaluate("test")


if __name__ == '__main__':
    # 建立解析对象，在main里面接受命令行传入的参数，然后做训练
    parser = argparse.ArgumentParser()
    # 给实例增加属性
    parser.add_argument("--task", default=None, required=True, type=str, help="The name of the task to train")
    parser.add_argument("--model_dir", default=None, required=True, type=str, help="Path to save, load model")
    parser.add_argument("--data_dir", default="./data", type=str, help="The input data dir")
    parser.add_argument("--intent_label_file", default="intent_label.txt", type=str, help="Intent Label file")
    parser.add_argument("--model_type", default="bert", type=str, help="Model type selected in the list: " + ", ".join(MODEL_CLASSES.keys()))

    parser.add_argument('--seed', type=int, default=1234, help="random seed for initialization")
    parser.add_argument("--train_batch_size", default=32, type=int, help="Batch size for training.")
    parser.add_argument("--eval_batch_size", default=64, type=int, help="Batch size for evaluation.")
    parser.add_argument("--max_seq_len", default=50, type=int, help="The maximum total input sequence length after tokenization.")
    parser.add_argument("--learning_rate", default=5e-5, type=float, help="The initial learning rate for Adam.")
    parser.add_argument("--num_train_epochs", default=10.0, type=float, help="Total number of training epochs to perform.")
    parser.add_argument("--weight_decay", default=0.0, type=float, help="Weight decay if we apply some.")
    parser.add_argument('--gradient_accumulation_steps', type=int, default=1,
                        help="Number of updates steps to accumulate before performing a backward/update pass.")
    parser.add_argument("--adam_epsilon", default=1e-8, type=float, help="Epsilon for Adam optimizer.")
    parser.add_argument("--max_grad_norm", default=1.0, type=float, help="Max gradient norm.")
    parser.add_argument("--max_steps", default=-1, type=int, help="If > 0: set total number of training steps to perform. Override num_train_epochs.")
    parser.add_argument("--warmup_steps", default=0, type=int, help="Linear warmup over warmup_steps.")
    parser.add_argument("--dropout_rate", default=0.1, type=float, help="Dropout for fully-connected layers")

    parser.add_argument('--logging_steps', type=int, default=200, help="Log every X updates steps.")
    parser.add_argument('--save_steps', type=int, default=200, help="Save checkpoint every X updates steps.")

    parser.add_argument("--do_train", action="store_true", help="Whether to run training.")
    parser.add_argument("--do_eval", action="store_true", help="Whether to run eval on the test set.")
    parser.add_argument("--no_cuda", action="store_true", help="Avoid using CUDA when available")

    parser.add_argument("--ignore_index", default=0, type=int,
                        help='Specifies a target value that is ignored and does not contribute to the input gradient')

    # linear 层
    parser.add_argument(
        "--linear_learning_rate", default=5e-5, type=float, help="The initial learning rate for CRF layer."
    )

    # 属性给与args实例:把parser中设置的所有"add_argument"给返回到args子类实例当中，
    # 那么parser中增加的属性内容都会在args实例中，使用即可。
    args = parser.parse_args()
    # 设置模型名字与路径
    args.model_name_or_path = MODEL_PATH_MAP[args.model_type]
    # 调用主程序
    main(args)
