﻿@[toc]
### 引言
&emsp;&emsp;这一节学习BERT模型如何在句子分类任务上进行微调。项目代码框架如下：
![在这里插入图片描述](https://img-blog.csdnimg.cn/669c870bf0ad4024b7fc9ec51e0642de.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NjY0OTA1Mg==,size_16,color_FFFFFF,t_70)
争取做到每行代码有注释！！！
### 一、项目环境配置
- python>=3.6
- torch==1.6.0
- transformers==3.0.2
- seqeval==0.0.12
- pytorch-crf==0.7.2

### 二、数据集介绍
|       | Train  | Dev | Test | Intent Labels |
| ----- | ------ | --- | ---- | ------------- | 
| ATIS  | 4,478  | 500 | 893  | 21            |

- 标签的数量基于训练数据集
- 对于仅在 dev 和 test数据集中显示的意图，为标签添加 `UNK`

&emsp;&emsp;我们这一节使用atis数据，数据集由训练集、验证集、测试集组成
![在这里插入图片描述](https://img-blog.csdnimg.cn/13ae6263c1234612a8589d6149c9c149.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NjY0OTA1Mg==,size_16,color_FFFFFF,t_70)
`seq.in`文件：每一行是一个文本
![在这里插入图片描述](https://img-blog.csdnimg.cn/101401a070ad422086304673acbe542b.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NjY0OTA1Mg==,size_16,color_FFFFFF,t_70)
`label`文件：每一行一个意图标签
![在这里插入图片描述](https://img-blog.csdnimg.cn/057e740f50b44c5c827b88f6700924cc.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NjY0OTA1Mg==,size_16,color_FFFFFF,t_70)
`intent_label.txt`文件：这个文件是由`vocab_process.py`文件生成的意图标签统计文件，一共22个，基于训练数据集标签文件的21个，再加上两个额外标签“UNK”

### 三、代码介绍
`data_loader.py`文件：这个文件的功能是将文本文件转化成InputExample类数据，并将输入样本转化为bert能够读取的InputFeatures类数据，最后保存至cache文件中，方便下次快速加载。
`utils.py`文件:封装了很多实用程序，方便统一调用
`trainer.py`文件：定义了任务的训练与评估以及保存模型与加载模型
`main.py`文件：用于模型的训练与评估
`predict.py`文件：用于模型的预测

### 四、测试结果
#### 1.代码执行流程
&emsp;&emsp;在命令行输入：

```python
python bert_finetune_cls/main.py --data_dir bert_finetune_cls/data/ --task atis --model_type bert --model_dir bert_finetune_cls/experiments/outputs/clsbert_0 --do_train --do_eval --train_batch_size 8 --num_train_epochs 2  --linear_learning_rate 5e-4
```
**测试结果为**:

```python
intent_acc = 0.888
loss = 0.4982787650078535
```
#### 2. 预测流程
&emsp;&emsp;在命令行输入

```python
python bert_finetune_cls/predict.py --input_file bert_finetune_cls/data/atis/test/seq.in --output_file bert_finetune_cls/experiments/outputs/clsbert_0/atis_test_predicted.txt --model_dir bert_finetune_cls/experiments/outputs/clsbert_0
```
***
博客链接：https://blog.csdn.net/weixin_46649052/article/details/119180940
