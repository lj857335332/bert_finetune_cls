import os


def vocab_process(data_dir):
    """
    获取意图标签
    :param data_dir: label
    :return:
    """
    # 输出文件名
    intent_label_vocab = 'intent_label.txt'
    # 训练文件路径拼接
    train_dir = os.path.join(data_dir, 'train')
    # intent
    with open(os.path.join(train_dir, 'label'), 'r', encoding='utf-8') as f_r, \
            open(os.path.join(data_dir, intent_label_vocab), 'w', encoding='utf-8') as f_w:
        intent_vocab = set()
        # 循环遍历训练label中的每一个意图标签
        for line in f_r:
            line = line.strip()
            intent_vocab.add(line)
        # 额外标签
        additional_tokens = ["UNK"]
        for token in additional_tokens:
            f_w.write(token + '\n')

        intent_vocab = sorted(list(intent_vocab))
        for intent in intent_vocab:
            f_w.write(intent + '\n')


if __name__ == "__main__":
    vocab_process(r'atis')
